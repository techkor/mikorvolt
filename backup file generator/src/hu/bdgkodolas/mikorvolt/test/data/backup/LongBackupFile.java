package hu.bdgkodolas.mikorvolt.test.data.backup;
import java.util.Random;
import java.util.Scanner;
import java.io.PrintStream;
import java.io.IOException;

public class LongBackupFile {

	public static String GetRandomString(int length) {
		char[] chars = "aábcdeéfghiíjklmnoóöőpqrstuúüűvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		return sb.toString();
	}

	public static String GetTheme() {
		return GetRandomString (16);
	}

	public static String GetYear() {
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		int year = random.nextInt(2017);
		sb.append(year);

		return sb.toString();
	}

	public static String GetMonth() {
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		int month = random.nextInt(12) + 1;
		sb.append(month);

		return sb.toString();
	}

	public static String GetDay() {
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		int day = random.nextInt(31) + 1;
		sb.append(day);

		return sb.toString();
	}

	public static String GetCentury() {
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		int day = random.nextInt(21) + 1;
		sb.append(day);

		return sb.toString();
	}

	public static int GetTimeType() {
		Random random = new Random();
		int type = random.nextInt(4);

		return type;
	}

	public static String GetDate() {
		int type = GetTimeType();
		String date = "";
		if (type == 0) {
			date = "c" + GetCentury();
		} else if (type == 1) {
			date = "y" + GetYear();
		} else if (type == 2) {
			date = "m" + GetYear() + "." + GetMonth() + ".";
		} else {
			date = "d" + GetYear() + "." + GetMonth() + "." + GetDay() + ".";
		}
		return date;
	}

	public static String GenerateFileContent (int lineCount) {
		StringBuilder sb = new StringBuilder();
		sb.append ("!tothekartboerprods 1.1!");
		sb.append ("\n");

		sb.append ("#");
		sb.append (GetTheme());
		sb.append ("\n");

		for (int i = 0; i < lineCount; i++) {
			sb.append (">");
			sb.append (GetRandomString(10));
			sb.append (" : ");
			sb.append (GetDate());
			sb.append ("\n");
		}
		return sb.toString ();
	}

	public static void main(String[] args) {
		System.out.println("Minden szoveg 10 hosszu");
		System.out.println("A generalt file neve Test.txt");
		System.out.println("Csak 1 tema van");
		System.out.println("Hany sorbol alljon?");
		System.out.println("(A temanev nelkul es a cim nelkul)");

		Scanner sc = new Scanner(System.in);
		int scanner = sc.nextInt();
		String fileContent = GenerateFileContent(scanner);

		try {
			PrintStream ki = new PrintStream("Test.txt", "UTF-8");
			ki.println(fileContent);
			ki.close();
		} catch(IOException e) {
			System.out.println("Error");
		}
	}
}