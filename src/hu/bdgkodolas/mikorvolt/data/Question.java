package hu.bdgkodolas.mikorvolt.data;

public class Question {

    private static int id;
    private String name;
    private HistoricalDate answer;

    //...................................................
    public Question(String name, HistoricalDate answer){
        id = id + 1;
        this.name = name;
        this.answer = answer;
    }

    //....................................................
    public String getName(){
        return name;
    }

    public HistoricalDate getAnswer(){
        return answer;
    }


    //........................................................
    public static void main(String[] args){
        Question q1 = new Question("Mikor volt a trianoni békeszerződés aláírása?", new HistoricalDate(1920,6,4));
        System.out.println(q1.getName());
        System.out.println(q1.getAnswer());

        Question q2 = new Question("Mikor fogadták el az áprilisi törvényeket?", new HistoricalDate(1848,4));
        System.out.println(q2.getName());
        System.out.println(q2.getAnswer());

        Question q3 = new Question("Melyik évben adta ki II.András az Aranybullát?", new HistoricalDate(1222, true));
        System.out.println(q3.getName());
        System.out.println(q3.getAnswer());

        Question q4 = new Question("Melyik évszázadban volt a tatárjárás?", new HistoricalDate(13, true));
        System.out.println(q4.getName());
        System.out.println(q4.getAnswer());
    }

}

