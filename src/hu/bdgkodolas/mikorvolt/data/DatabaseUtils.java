package hu.bdgkodolas.mikorvolt.data;


import java.util.Map;
import java.util.Random;

public class DatabaseUtils {
	private static Random random = new Random();

	/**
	 * Generates a unique random id
	 *
	 * @return
	 */
	protected static long generateId(Map<Long, ? extends Object> map) {
		synchronized (map) {
			long id;
			do {
				id = random.nextLong();
			} while (map.get(id) != null);
			return id;
		}
	}
}
