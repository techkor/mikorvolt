package hu.bdgkodolas.mikorvolt.data;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class Backup {

	public static void backup(String fileName) {
		synchronized (TopicDatabase.class) {
			synchronized (UserDatabase.class) {
				try {
					PrintStream ki = new PrintStream(fileName, "UTF-8");
					ki.println("!tothekartboerbrods 1.0!");
					Topic[] topics = TopicDatabase.getTopics();
					for (Topic topic : topics) {
						ki.println("#" + topic.getName());
						ki.println("/" + topic.getDescription());
						ki.print("@");
						String[] keywords = topic.getKeywords();
						for (String keyword : keywords) {
							ki.print(keyword + "|");
						}
						ki.println();
						ki.println("]" + topic.id);
						ki.println("[" + topic.getOwner().getName());
						Question[] questions = topic.getQuestions();
						for (Question question : questions) {
							ki.print(">" + question.getName() + " : ");
							HistoricalDate answer = question.getAnswer();
							switch (answer.getMode()) {
								case HistoricalDate.MODE_YEAR_MONTH_DAY:
									ki.println("d" + answer.getYear() + "." + answer.getMonth() + "." + answer.getDay()
													   + ".");
									break;
								case HistoricalDate.MODE_YEAR_MONTH:
									ki.println("m" + answer.getYear() + "." + answer.getMonth() + ".");
									break;
								case HistoricalDate.MODE_YEAR:
									ki.println("y" + answer.getYear());
									break;
								case HistoricalDate.MODE_CENTURY:
									ki.println("c" + answer.getCentury());
									break;
							}
						}

					}
					User[] users = UserDatabase.getUsers();
					for (User user : users) {
						ki.println("&" + user.getName());
						ki.println("~" + user.getPassword());
					}
					ki.println();

				} catch (IOException e) { // Ha nem lehet létrehozni a fájlt vagy nem lehet bele írni

					System.out.println(
							"Az integrált pluralista szocializmusban az alternatív demokrácia objektív frekventáltsága"
									+ " stagnál");
				}
			}
		}
	}


	public static void restore(String fileName) {
		synchronized (TopicDatabase.class) {
			synchronized (UserDatabase.class) {
				// TODO clear everything
				try {
					Scanner sc = new Scanner(new FileInputStream(fileName), "UTF-8");
					sc.nextLine();
					String line = sc.nextLine();
					while (line.length() != 0) {
						if (line.charAt(0) == '#') {
							String topicname = line.substring(1);
							line = sc.nextLine();
							if (line.charAt(0) == '/') {
								String description = line.substring(1);
								line = sc.nextLine();
								if (line.charAt(0) == '@') {
									String[] keywords = line.substring(1).split("\\|");
									line = sc.nextLine();
									if (line.charAt(0) == ']') {
										long id = Long.parseLong(line.substring(1));
										line = sc.nextLine();
										if (line.charAt(0) == '[') {
											String ownerName = line.substring(1);
											Topic kacsa = new Topic(id, topicname, description, keywords);
											kacsa.ownerName = ownerName;
											line = sc.nextLine();
											while (line.length() != 0 && line.charAt(0) == '>') {
												String[] darabok = line.substring(1).split(" : ");
												HistoricalDate datum = null;
												switch (darabok[1].charAt(0)) {
													case 'c':
														datum = new HistoricalDate(
																Integer.parseInt(darabok[1].substring(1)), false);
														break;
													case 'y':
														datum = new HistoricalDate(
																Integer.parseInt(darabok[1].substring(1)), true);
														break;
													case 'm':
														String[] darabka = darabok[1].substring(1).split("\\.");
														datum = new HistoricalDate(
																Integer.parseInt(darabka[0]),
																Integer.parseInt(darabka[1])
														);
														break;
													case 'd':
														String[] darabkak = darabok[1].substring(1).split("\\.");
														datum = new HistoricalDate(
																Integer.parseInt(darabkak[0]),
																Integer.parseInt(darabkak[1]),
																Integer.parseInt(darabkak[2])
														);
														break;
												}
												Question question = new Question(darabok[0], datum);
												kacsa.addQuestion(question);
												line = sc.nextLine();
											}
											TopicDatabase.addTopic(kacsa);
										}
									}
								}
							}
						}

						if (line.length() != 0 && line.charAt(0) == '&') {
							String username = line.substring(1);
							line = sc.nextLine();
							if (line.charAt(0) == '~') {
								String password = line.substring(1);
								User user = new User(username, password, true);
								UserDatabase.addUser(user);
								line = sc.nextLine();
							}
						}
					}

					for (Topic topic : TopicDatabase.getTopics())
						topic.owner = UserDatabase.getUser(topic.ownerName);

				} catch (IOException e) { // Ha fájl nem elérhető:

					System.out.println("Nem található a fájl");

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

}
