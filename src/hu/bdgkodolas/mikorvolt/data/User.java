package hu.bdgkodolas.mikorvolt.data;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class User {
	private String name;
	private String passwordHash;

	public User(String name0, String password, boolean hashed) {
		name = name0;
		updatePassword(password, hashed);
	}

	static String sha1(String input) {
		try {
			MessageDigest mDigest = MessageDigest.getInstance("SHA1");
			byte[] result = mDigest.digest(input.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < result.length; i++) {
				sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
			}

			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}

	public void updatePassword(String password, boolean hashed) {
		if (hashed)
			this.passwordHash = password;
		else
			this.passwordHash = sha1(password);
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return passwordHash;
	}

	public boolean checkPassword(String password) {
		String bppasswordHash = sha1(password);
		return passwordHash.equals(bppasswordHash);
	}


}