package hu.bdgkodolas.mikorvolt.webserver.game;


import hu.bdgkodolas.mikorvolt.data.Topic;

import java.util.Map;

public abstract class Game {

	/**
	 * Should be overriden
	 */
	public static final String NAME = null;

	private Topic topic;

	public Game(Topic topic) {
		this.topic = topic;
	}

	public Topic getTopic() {
		return topic;
	}

	public abstract boolean isFinished();

	public abstract String serve(Map<String, String> params);

	public abstract String getName();

}
