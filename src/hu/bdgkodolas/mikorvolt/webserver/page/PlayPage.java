package hu.bdgkodolas.mikorvolt.webserver.page;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;
import hu.bdgkodolas.mikorvolt.data.Topic;
import hu.bdgkodolas.mikorvolt.data.TopicDatabase;
import hu.bdgkodolas.mikorvolt.webserver.Page;
import hu.bdgkodolas.mikorvolt.webserver.Session;
import hu.bdgkodolas.mikorvolt.webserver.game.*;

import java.util.Map;

public class PlayPage implements Page {

	public static final Page INSTANCE = new PlayPage();

	private static final String KEY_TOPICNAME = "%TOPICNAME%";
	private static final String KEY_GAMENAME  = "%GAMENAME%";

	private static final String EXIT_BUTTON =
			"<form action=\".\" method=\"post\">"
			+ "<input type=\"hidden\" name=\"gameMode\" value=\"-1\">"
			+ "<input type=\"submit\" value=\"Kilépés\">"
			+ "</form>";
	private static final String HEADER      =
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n"
			+ "<html>\n"
			+ "\t<head>\n"
			+ "\t\t<title>Mikorvolt - " + KEY_TOPICNAME + "</title>\n"
			+ "\t\t<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\n"
			+ "\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
			+ "\t\t<meta name=\"description\" content=\"\">\n"
			+ "\t\t<meta name=\"author\" content=\"\">\n"
			+ "\t\t<link href=\"css/style.css\" rel=\"stylesheet\">\n"
			+ "\t\t<link href=\"files/favicon.png\" rel=\"icon\">\n"
			+ "\t\t<style>\n"
			+ "\t\t\t.bigbutton{\n"
			+ "\t\t\t\ttext-decoration: none;\n"
			+ "\t\t\t\tfont-family: Agency FB;\n"
			+ "\t\t\t\tcolor: white;\n"
			+ "\t\t\t\tborder-radius: 12px;\n"
			+ "\t\t\t\theight: 36px;\n"
			+ "\t\t\t\twidth: 300px;\n"
			+ "\t\t\t\tbackground-color: rgb(222, 80, 37);\n"
			+ "\t\t\t\tborder: 5px solid;\n"
			+ "\t\t\t\ttext-align: center;\n"
			+ "\t\t\t\tmargin-left: auto;\n"
			+ "\t\t\t\tmargin-right: auto;\n"
			+ "\t\t\t\theight: 40px;\n"
			+ "\t\t\t}\n"
			+ "\t\t\t#gameContainer{\n"
			+ "\t\t\t\tbackground-color: white;\n"
			+ "\t\t\t\tborder-radius: 24px;\n"
			+ "\t\t\t\theight: 60%;\n"
			+ "\t\t\t\twidth: 80%;\n"
			+ "\t\t\t\tborder: 5px solid;\n"
			+ "\t\t\t\tborder-color: white;\n"
			+ "\t\t\t\tpadding: 12px;\n"
			+ "\t\t\t}\n"
			+ "\t\t</style>\n"
			+ "\t</head>\n"
			+ "\t<body style=\"background-image: url(../../../files/history-wallpaper-9.jpg);"
			+ "background-position:center top;\">\n"
			+ "\t\t<br><br>\n"
			+ "\t\t<table style=\"text-align: left; width: 100%; height: 64px;\" border=\"0\" "
			+ "cellpadding=\"0\" "
			+ "cellspacing=\"5\">\n"
			+ "\t\t\t<tbody>\n"
			+ "\t\t\t\t<tr>\n"
			+ "\t\t\t\t\t<td>\n"
			+ "\t\t\t\t\t\t<a href=\"../\">\n"
			+ "\t\t\t\t\t\t\t<img style=\"border: 0px solid ; width: 48px; height: 48px;\" alt=\"\" "
			+ "title=\"Visszalépés\" src=\"../../../files/back.png\">\n"
			+ "\t\t\t\t\t\t</a>\n"
			+ "\t\t\t\t\t</td>\n"
			+ "\t\t\t\t\t<td>\n"
			+ "\t\t\t\t\t\t<big style=\"color: rgb(222, 80, 37);\">\n"
			+ "\t\t\t\t\t\t\t<big><big><big><big>\n"
			+ "\t\t\t\t\t\t\t\t<span style=\"font-family: Agency FB;\">&nbsp;&nbsp;\n"
			+ "\t\t\t\t\t\t\t\t    <span style=\"font-weight: bold;\">" + KEY_TOPICNAME + " - " + KEY_GAMENAME
			+ "</span>                 \n"
			+ "\t\t\t\t\t\t\t\t</span>\n"
			+ "\t\t\t\t\t\t\t</big></big></big></big>\n"
			+ "\t\t\t\t\t\t</big>\n"
			+ "\t\t\t\t\t</td>\n"
			+ "\t\t\t\t</tr>\n"
			+ "\t\t\t</tbody>\n"
			+ "\t\t</table>\n"
			+ "\t\t<br><br><br>\n"
			+ "\t\t<center>\n"
			+ "\t\t\t<div id=\"gameContainer\">\n";
	private static final String FOOTER      =
			"\t\t\t</div>\n"
			+ "\t\t</center>\n"
			+ "\t</body>\n"
			+ "</html>";

	private static final String     PREFIX     = TopicPage.PREFIX;
	private static final String     POSTFIX    = "/play/";
	private static final GameMode[] GAME_MODES = new GameMode[] {
			new GameMode(LearningGame.class),
			new GameMode(SameYearGame.class)
	};

	private PlayPage() {
	}

	private static Response chooseGame(Topic topic) {
		String html = HEADER
					  + "Válassz játékot!<br>\n<form id=\"gameModeForm\" action=\".\" method=\"post\"><input "
					  + "type=\"hidden\" id=\"gameMode\" name=\"gameMode\" value=\"-1\">";
		for (int i = 0; i < GAME_MODES.length; ++i)
			html += "<a href=\"javascript:{"
					+ "document.getElementById('gameMode').value='" + i + "';"
					+ "document.getElementById('gameModeForm').submit();"
					+ "}\">" + GAME_MODES[i].NAME + "</a><br>";
		html += "</form>" + FOOTER;
		html = html.replace(KEY_TOPICNAME, topic.getName())
				.replace(KEY_GAMENAME, "Játékválasztás");
		return NanoHTTPD.newFixedLengthResponse(html);
	}

	private static Response gamePage(Game game, Map<String, String> parms) {
		String html = HEADER + game.serve(parms) + EXIT_BUTTON + FOOTER;
		html = html.replace(KEY_TOPICNAME, game.getTopic().getName())
				.replace(KEY_GAMENAME, game.getName());
		return NanoHTTPD.newFixedLengthResponse(html);
	}

	@Override
	public boolean matches(String uri) {
		return TopicPageUtils.checkTopicId(TopicPageUtils.getTopicId(uri, PREFIX, POSTFIX));
	}

	@Override
	public Response serve(IHTTPSession request, Session session) throws Exception {

		Map<String, String> parms = request.getParms();
		String gameModeString = parms.get("gameMode");
		Topic topic = TopicDatabase.getTopic(TopicPageUtils.getTopicId(request.getUri(),
																	   PREFIX, POSTFIX));

		if (gameModeString != null) { // gameMode was sent

			try { // Start a new game!
				int gameMode = Integer.parseInt(parms.get("gameMode"));

				if (gameMode == -1)
					return chooseGame(topic);
				else if (gameMode >= 0 && gameMode < GAME_MODES.length) // validate gameMode
					session.currentGame =
							GAME_MODES[gameMode].CLASS.getConstructor(Topic.class)
									.newInstance(topic);
				else
					return NotFoundPage.INSTANCE.serve(request, session);

			} catch (Exception e) {
				return NotFoundPage.INSTANCE.serve(request, session);
			}
		}

		if (session.currentGame != null) // A game is in progress
			return gamePage(session.currentGame, request.getParms());

		// let's select a game
		return chooseGame(topic);
	}

	private static class GameMode {
		private final String                NAME;
		private final Class<? extends Game> CLASS;

		private GameMode(Class<? extends Game> clazz) {
			String name;
			try {
				name = (String) clazz.getField("NAME").get(null);
			} catch (NoSuchFieldException | IllegalAccessException e) {
				name = null;
				System.err.println("Hibás Game");
				e.printStackTrace();
			}
			NAME = name;
			CLASS = clazz;
		}
	}
}
