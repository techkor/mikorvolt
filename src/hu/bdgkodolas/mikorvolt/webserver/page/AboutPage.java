package hu.bdgkodolas.mikorvolt.webserver.page;


import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;
import hu.bdgkodolas.mikorvolt.webserver.Page;
import hu.bdgkodolas.mikorvolt.webserver.Session;

public class AboutPage implements Page {

	public static final  Page   INSTANCE = new AboutPage();
	private static final String URI      = "/about/";
	private static final String HTML     =
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n"
			+ "<html>\n"
			+ "\t<head>\n"
			+ "\t\t<meta charset=\"utf-8\">\n"
			+ "\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
			+ "\t\t<meta name=\"description\" content=\"\">\n"
			+ "\t\t<meta name=\"author\" content=\"\">\n"
			+ "\t\t<title>Mikorvolt - Rólunk</title>\n"
			+ "\t\t<link href=\"../css/style.css\" rel=\"stylesheet\">\n"
			+ "\t\t<link href=\"../files/favicon.png\" rel=\"icon\">\n"
			+ "\t\t<style>\n"
			+ "\t\t\t.bigbutton{\n"
			+ "\t\t\t\ttext-decoration: none;\n"
			+ "\t\t\t\tfont-family: Agency FB;\n"
			+ "\t\t\t\tcolor: white;\n"
			+ "\t\t\t\tborder-radius: 12px;\n"
			+ "\t\t\t\theight: 36px;\n"
			+ "\t\t\t\twidth: 300px;\n"
			+ "\t\t\t\tbackground-color: rgb(222, 80, 37);\n"
			+ "\t\t\t\tborder: 5px solid;\n"
			+ "\t\t\t\ttext-align: center;\n"
			+ "\t\t\t\tmargin-left: auto;\n"
			+ "\t\t\t\tmargin-right: auto;\n"
			+ "\t\t\t\theight: 40px;\n"
			+ "\t\t\t}\n"
			+ "\t\t</style>\n"
			+ "\t</head>\n"
			+ "\t<body>\n"
			+ "\t\t<table style=\"text-align: left; width: 100%; margin-left: auto; margin-right: auto; height: 150px;"
			+ "\" background=\"../files/history-wallpaper-9.jpg\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\n"
			+ "\t\t\t<tbody>\n"
			+ "\t\t\t\t<tr>\n"
			+ "\t\t\t\t\t<td>\n"
			+ "\t\t\t\t\t\t<h1> <a href=\"../\"><img style=\"border: 0px solid ; width: 48px; height: 48px;\" alt=\"\""
			+ " title=\"Visszalépés\" src=\"../files/back.png\"></a> &nbsp;\n"
			+ "\t\t\t\t\t\t\t<big>\n"
			+ "\t\t\t\t\t\t\t\t<span style=\"color: rgb(222, 80, 37); font-family: Agency FB;\">Rólunk</span>\n"
			+ "\t\t\t\t\t\t\t</big>\n"
			+ "\t\t\t\t\t\t</h1>\n"
			+ "\t\t\t\t\t</td>\n"
			+ "\t\t\t\t</tr>\n"
			+ "\t\t\t</tbody>\n"
			+ "\t\t</table>\n"
			+ "\t\t<br>\n"
			+ "\t\t<center>\n"
			+ "\t\t\t<table style=\"width: 70%; text-align: left; margin-left: auto; margin-right: auto;\" "
			+ "border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\n"
			+ "\t\t\t\t<tbody>\n"
			+ "\t\t\t\t\t<tr>\n"
			+ "\t\t\t\t\t\t<td>\n"
			+ "\t\t\t\t\t\t\t<span style=\"font-family: Arial; text-align: left;\">\n"
			+ "\t\t\t\t\t\t\t\t<p>\n"
			+ "\t\t\t\t\t\t\t\t&nbsp;&nbsp; A Mikorvolt egy történelemgyakorló szoftver.\n"
			+ "\t\t\t\t\t\t\t\tAz évszámokat lehet vele megtanulni.\n"
			+ "\t\t\t\t\t\t\t\tAgyfejlesztő tesztekkel, témakörökbe rendezhető egyéni kérdésekkel és áttekinthető "
			+ "külsővel segít az érettségire és dolgozatokra, vizsgákra való felkészülésekben.\n"
			+ "\t\t\t\t\t\t\t\tA témakörökbe rendezett kérdések létrehozása után többféle típusú teszt futtatható le "
			+ "az adott témakörből.\n"
			+ "\t\t\t\t\t\t\t\tA kérdések után a felhasználó visszajelzést kap az eredményről, látja a helyes és a "
			+ "helytelen válaszokat. Így folyamatosan fejlesztheti tudását.\n"
			+ "\t\t\t\t\t\t\t\t</p>\n"
			+ "\t\t\t\t\t\t\t\t<br>\n"
			+ "\t\t\t\t\t\t\t\t<br>\n"
			+ "\t\t\t\t\t\t\t\t<p>\n"
			+ "\t\t\t\t\t\t\t\t&nbsp;&nbsp; A Mikorvoltot egy gimnáziumi szakkörcsoport készítette, a budapesti "
			+ "Berzsenyi Dániel Gimnázium Kódolás szakköre.\n"
			+ "\t\t\t\t\t\t\t\tA szoftver több hónapon át tartó projektmunka eredménye. Elkészítésében részt "
			+ "vettek:<br>\n"
			+ "\t\t\t\t\t\t\t\t&nbsp;&nbsp;&nbsp;Ajanidisz András, Bernhardt Milán, Boér Máté, Ekart Csaba, Füstös "
			+ "Gergő, Hornák Bence, Józsa Richárd, Kiss Dániel, Kovács Máté, Kubik Dominik, Kugyella Noémi, Oláh "
			+ "Gábor, Tóth Sebestyén, Végh Márton.\n"
			+ "\t\t\t\t\t\t\t\t<br>\n"
			+ "\t\t\t\t\t\t\t\t</p>\n"
			+ "\t\t\t\t\t\t\t</span>\n"
			+ "\t\t\t\t\t\t</td>\n"
			+ "\t\t\t\t\t</tr>\n"
			+ "\t\t\t\t</tbody>\n"
			+ "\t\t\t</table>\n"
			+ "\t\t</center>\n"
			+ "\t\t<br>\n"
			+ "\t\t<table class=\"bigbutton\" valign=\"middle\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\">\n"
			+ "\t\t\t<tbody>\n"
			+ "\t\t\t\t<tr>\n"
			+ "\t\t\t\t\t<td>\n"
			+ "\t\t\t\t\t\t<p>\n"
			+ "\t\t\t\t\t\t\t<big><big><big>\n"
			+ "\t\t\t\t\t\t\t"
			+ "\t<a href=\"http://kodolas.hu\" target=\"_blank\" style=\"text-decoration: none; color: white;\"> A "
			+ "szakkör honlapja</a>\n"
			+ "\t\t\t\t\t\t\t</big></big></big>\n"
			+ "\t\t\t\t\t\t</p>\n"
			+ "\t\t\t\t\t</td>\n"
			+ "\t\t\t\t</tr>\n"
			+ "\t\t\t</tbody>\n"
			+ "\t\t</table>\n"
			+ "\t</body>\n"
			+ "</html>\n";

	private AboutPage() {
	}

	@Override
	public boolean matches(String uri) {
		return uri.equals(URI);
	}

	@Override
	public Response serve(IHTTPSession request, Session session) {
		return NanoHTTPD.newFixedLengthResponse(HTML);
	}
}
